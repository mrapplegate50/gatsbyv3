---
title: Yummy Beef
date: "2021-03-02T22:12:03.284Z"
description: "I ate Wagyu Beef for the first time"
---
![Wagyu Beef](./wagyu-beef.jpg)

I had [Wagyu](https://en.wikipedia.org/wiki/Wagyu) beef for the first time today. I'm not sure of the quality since it was at [Red Lobster](https://www.redlobster.com), but I'm standing by my announcement. Oh, by the way, it was tasty.

<StaticImage src="https://placekitten.com/800/600" alt="Kitten" />
