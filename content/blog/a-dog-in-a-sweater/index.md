---
title: A dog in a sweater, duh
date: "2021-03-11T21:44:14+00:00"
description: "A fashion forward thinking dog"
---

Just a dog in a sweater. Nothing to see here. Move along.

<picture>
<source
media="(max-width: 767px)"
sizes="(max-width: 1534px) 100vw, 1534px"
srcset="
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_200.jpg 200w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_540.jpg 540w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_862.jpg 862w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_1049.jpg 1049w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_1221.jpg 1221w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_1395.jpg 1395w,
Pup-sweater_vmvcah_ar_1_1,c_fill,g_auto__c_scale,w_1534.jpg 1534w">
<source
media="(min-width: 768px) and (max-width: 991px)"
sizes="(max-width: 1983px) 70vw, 1388px"
srcset="
Pup-sweater_vmvcah_ar_4_3,c_fill,g_auto__c_scale,w_538.jpg 538w,
Pup-sweater_vmvcah_ar_4_3,c_fill,g_auto__c_scale,w_898.jpg 898w,
Pup-sweater_vmvcah_ar_4_3,c_fill,g_auto__c_scale,w_1195.jpg 1195w,
Pup-sweater_vmvcah_ar_4_3,c_fill,g_auto__c_scale,w_1388.jpg 1388w">
<source
media="(min-width: 992px) and (max-width: 1199px)"
sizes="(max-width: 2400px) 60vw, 1440px"
srcset="
Pup-sweater_vmvcah_ar_16_9,c_fill,g_auto__c_scale,w_596.jpg 596w,
Pup-sweater_vmvcah_ar_16_9,c_fill,g_auto__c_scale,w_1031.jpg 1031w,
Pup-sweater_vmvcah_ar_16_9,c_fill,g_auto__c_scale,w_1377.jpg 1377w,
Pup-sweater_vmvcah_ar_16_9,c_fill,g_auto__c_scale,w_1440.jpg 1440w">
<img
sizes="(max-width: 7000px) 40vw, 2800px"
srcset="
Pup-sweater_vmvcah_c_scale,w_480.jpg 480w,
Pup-sweater_vmvcah_c_scale,w_849.jpg 849w,
Pup-sweater_vmvcah_c_scale,w_1130.jpg 1130w,
Pup-sweater_vmvcah_c_scale,w_1351.jpg 1351w,
Pup-sweater_vmvcah_c_scale,w_1483.jpg 1483w,
Pup-sweater_vmvcah_c_scale,w_1556.jpg 1556w,
Pup-sweater_vmvcah_c_scale,w_1721.jpg 1721w,
Pup-sweater_vmvcah_c_scale,w_1874.jpg 1874w,
Pup-sweater_vmvcah_c_scale,w_2009.jpg 2009w,
Pup-sweater_vmvcah_c_scale,w_2139.jpg 2139w,
Pup-sweater_vmvcah_c_scale,w_2142.jpg 2142w,
Pup-sweater_vmvcah_c_scale,w_2365.jpg 2365w,
Pup-sweater_vmvcah_c_scale,w_2399.jpg 2399w,
Pup-sweater_vmvcah_c_scale,w_2531.jpg 2531w,
Pup-sweater_vmvcah_c_scale,w_2609.jpg 2609w,
Pup-sweater_vmvcah_c_scale,w_2684.jpg 2684w,
Pup-sweater_vmvcah_c_scale,w_2800.jpg 2800w"
src="Pup-sweater_vmvcah_c_scale,w_2800.jpg"
alt="">
</picture>

Plus a code test.
```js
/**
 * Get value out of string (e.g. rem => px)
 * If value is px strip the px part
 * If the input is already a number only return that value
 * @param {string | number} input
 * @param {number} [rootFontSize]
 * @return {number} Number without last three characters
 * @example removeLastThree('6rem') => 6
 */
const getValue = (input, rootFontSize = 16) => {
  if (typeof input === `number`) {
    return input / rootFontSize;
  }

  const isPxValue = input.slice(-2) === `px`;

  if (isPxValue) {
    return parseFloat(input.slice(0, -2));
  }

  return parseFloat(input.slice(0, -3));
};

// This is a little helper function
const helper = (a, b) => a + b;

// This is also a little helper function but this time with a really long one-line comment that should show some more details
const morehelper = (a, b) => a * b;

export { getValue, helper, morehelper };
```
